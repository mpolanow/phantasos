# How to Make Temporal Geospatial Predictions: Data Science Case Studies in Southeast Asia
## Outline
-----------------

1. Background / Introduction
    - What is Geography?
        - History of Geography
        - Relation to other fields (particularly Earth Science and Systems Science)
    - What is Data Science?
        - History of Data Science
        - Silicon Valley Invention
        - Academic vs Industry Context
    - Relation to other fields
    - What is the Relationship between Geography and Data Science?
1. Framework / Methods
    - Why do we need a framework?
        - What good are they?
    - What frameworks currently exist?
        - In general
        - Specific to temporal geospatial problems
    - What makes a good framework?
        - How do we measure the "goodness" of a framework
            - How many types of problems can it be applied to
            - How many people use it
            - How easy it is to learn and apply
            - How much impact it has on the world
    - What is the proposed framework?
        - How did it come about?
        - Where can it be applied?
        - What generalisations does it make?
1. Coastal Case Study
    - Describe the temporal geospatial problem
    - Specific background to this case study
        - Why are mangroves important?
1. Rural Case Study
    - Describe the temporal geospatial problem
    - Specific background to this case study
        - Why are landcover maps important?
1. Urban Case Study
    - Describe the temporal geospatial problem
    - Specific background to this case study
        - Why is dynamic vehicle routing important?
1. Synthesis / Framework Analysis
    - What is the framework?
    - How did it perform?
    - Strengths
    - Weaknesses
1. Conclusion / Critique
1. Bibliography
